/******************************
 * Submitted by: enter your first and last name and net ID
 * CS 3339 - Fall 2020, Texas State University
 * Project 5 Data Cache
 * Copyright 2020, all rights reserved
 * Updated by Lee B. Hinkle based on prior work by Martin Burtscher and Molly O'Neil
 ******************************/
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include "CacheStats.h"
using namespace std;

CacheStats::CacheStats() {
  cout << "Cache Config: ";
  if(!CACHE_EN) {
    cout << "cache disabled" << endl;
  } else {
    cout << (SETS * WAYS * BLOCKSIZE) << " B (";
    cout << BLOCKSIZE << " bytes/block, " << SETS << " sets, " << WAYS << " ways)" << endl;
    cout << "  Latencies: Lookup = " << LOOKUP_LATENCY << " cycles, ";
    cout << "Read = " << READ_LATENCY << " cycles, ";
    cout << "Write = " << WRITE_LATENCY << " cycles" << endl;
  }
  
  loads = 0;
  stores = 0;
  load_misses = 0;
  store_misses = 0;
  writebacks = 0;

  for (int i = 0; i < SETS; i ++){
    roundrobin[i] = 0;
  }

}

int CacheStats::access(uint32_t addr, ACCESS_TYPE type) {
  if(!CACHE_EN) { // cache is not enabled
    return (type == LOAD) ? READ_LATENCY : WRITE_LATENCY;
  }
  if (type == LOAD) loads++;
  else stores++;
  int index = (addr & 0x7) >> 5;
  uint32_t thistag = (addr & 0x3ff0) >> 8;

  bool miss;

  for (int i = 0; i < WAYS; i++){
    if (array[index][i].tag == thistag){ // hit
      miss = false;
      if (type == STORE){
        array[index][i].dirtyBit = true;
        writebacks++;
      }
      return LOOKUP_LATENCY;
    }
    miss = true;
  }

  if (miss == true){
  int r = roundrobin[index];
  roundrobin[index]++;
  roundrobin[index] %= WAYS;
  array[index][r].tag = thistag;
  array[index][r].validBit = true;

  if (type == STORE){
    store_misses++;
    if (array[index][r].dirtyBit == true){
      writebacks++;
      return READ_LATENCY + WRITE_LATENCY;
    }
    else {
      array[index][r].dirtyBit = true;
      return WRITE_LATENCY;
    }
  }
  else if (type == LOAD){
    load_misses++;
    if (array[index][r].dirtyBit == true){
      writebacks++;
      array[index][r].dirtyBit = false;
      return READ_LATENCY + WRITE_LATENCY;
      
    }
    else {
      return READ_LATENCY;
    }
  }
  }

  return 0;
}

void CacheStats::printFinalStats() {

  for (int i = 0; i < SETS; i++){
    for (int j = 0; j < WAYS; j++){
      if (array[i][j].dirtyBit == true)
      writebacks++;
    }
  }

  int accesses = loads + stores;
  int misses = load_misses + store_misses;
  cout << "Accesses: " << accesses << endl;
  cout << "  Loads: " << loads << endl;
  cout << "  Stores: " << stores << endl;
  cout << "Misses: " << misses << endl;
  cout << "  Load misses: " << load_misses << endl;
  cout << "  Store misses: " << store_misses << endl;
  cout << "Writebacks: " << writebacks << endl;
  cout << "Hit Ratio: " << fixed << setprecision(1) << 100.0 * (accesses - misses) / accesses;
  cout << "%" << endl;
}
