/******************************
 * Submitted by: Mary Kathryn Livingston mol9
 * CS 3339 - Fall 2020, Texas State University
 * Project 3 Pipelining
 * Copyright 2020, all rights reserved
 * Updated by Lee B. Hinkle based on prior work by Martin Burtscher and Molly O'Neil
 ******************************/
 
#include "Stats.h"

Stats::Stats() {
  cycles = PIPESTAGES - 1; // pipeline startup cost
  flushes = 0;
  bubbles = 0;
  stalls = 0;

  memops = 0;
  branches = 0;
  taken = 0;

  for(int i = IF1; i < PIPESTAGES; i++) {
    resultReg[i] = -1;
    resultStage[i] = -1;
    hazardStages[i] = 0;
  }

}

void Stats::clock(PIPESTAGE currentStage) {
  cycles++;
  // advance all pipeline flip-flops
  for(int i = WB; i > currentStage; i--) {
    resultReg[i] = resultReg[i-1];
    resultStage[i] = resultStage[i-1];
  }

  // inject a NOP in IF1
  resultReg[currentStage] = -1;
  resultStage[currentStage] = -1;
}

void Stats::registerSrc(int r, PIPESTAGE needed) {
  
  int cyclesTillNeed = (needed - ID);
  int valid;
  int bubs;

  for ( int i = EXE1; i < WB; i++){
    
    if ((resultReg[i] == r) && r != 0){
      hazardStages[i]++;
      valid = resultStage[i] - i;
      bubs = (resultStage[i]-i)-(needed-ID);

      while(bubs>0){
        bubble();
        bubs--;
      }
      break;
    }
  }  
}

void Stats::registerDest(int r, PIPESTAGE resultIsValid) {
  
  resultStage[ID] = resultIsValid;

  resultReg[ID] = r;

}

void Stats::flush(int count) { // count == how many ops to flush

  for(int i = 0; i < count; i++) {
    flushes++; clock(IF1);
  }

}

void Stats::bubble() {
  
bubbles++;
clock(EXE1);

}

void Stats::stall(int n) {

  while (n > 0){
    stalls++;
    cycles++;
    n--;
  }

}

void Stats::showPipe() {
  // this method is to assist testing and debug, please do not delete or edit
  // you are welcome to use it but remove any debug outputs before you submit
  cout << "              IF1  IF2 *ID* EXE1 EXE2 MEM1 MEM2 WB         #C      #B      #F" << endl; 
  cout << "  resultReg ";
  for(int i = 0; i < PIPESTAGES; i++) {
    cout << "  " << dec << setw(2) << resultReg[i] << " ";
  }
  cout << "   " << setw(7) << cycles << " " << setw(7) << bubbles << " " << setw(7) << flushes;
  cout << endl;
}
