/******************************
 * Submitted by: Mary Kathryn Livingston mol9
 * CS 3339 - Fall 2020, Texas State University
 * Project 3 Pipelining
 * Copyright 2020, all rights reserved
 * Updated by Lee B. Hinkle based on prior work by Martin Burtscher and Molly O'Neil
 ******************************/
 
#ifndef __STATS_H
#define __STATS_H
#include <iostream>
#include <iomanip>
#include "Debug.h"
#include "CacheStats.h"
#include <numeric>
using namespace std;

enum PIPESTAGE { IF1 = 0, IF2 = 1, ID = 2, EXE1 = 3, EXE2 = 4, MEM1 = 5, 
                 MEM2 = 6, WB = 7, PIPESTAGES = 8 };

class Stats {
  private:
    long long cycles;
    int flushes;
    int bubbles;
    int stalls;

    int memops;
    int branches;
    int taken;

    int resultReg[PIPESTAGES];
    int resultStage[PIPESTAGES];
    
    

  public:
    Stats();

    void clock(PIPESTAGE);

    int hazardStages[PIPESTAGES];

    void flush(int count);

    void stall(int n);

    void registerSrc(int r, PIPESTAGE n); // n = needed
    void registerDest(int r, PIPESTAGE v); // v = valid

    void countMemOp() { memops++; }
    void countBranch() { branches++; }
    void countTaken() { taken++; }
	
    void showPipe();

    // getters
    long long getCycles() { return cycles; }
    int getFlushes() { return flushes; }
    int getBubbles() { return bubbles; }
    int getStalls() {return stalls; }
    int getMemOps() { return memops; }
    int getBranches() { return branches; }
    int getTaken() { return taken; }
    int getRAWHazards() {return accumulate(hazardStages, hazardStages+(sizeof(hazardStages)/sizeof(hazardStages[0])), 0);}

  private:
    void bubble();
    
};

#endif
