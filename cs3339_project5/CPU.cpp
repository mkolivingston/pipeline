/******************************
 * Submitted by: Mary Kathryn Livingston mol9
 * CS 3339 - Fall 2020, Texas State University
 * Project 3 Pipelining
 * Copyright 2020, all rights reserved
 * Updated by Lee B. Hinkle based on prior work by Martin Burtscher and Molly O'Neil
 ******************************/
#include <math.h>
#include "CPU.h"
#include "Stats.h"

const string CPU::regNames[] = {"$zero","$at","$v0","$v1","$a0","$a1","$a2","$a3",
                                "$t0","$t1","$t2","$t3","$t4","$t5","$t6","$t7",
                                "$s0","$s1","$s2","$s3","$s4","$s5","$s6","$s7",
                                "$t8","$t9","$k0","$k1","$gp","$sp","$fp","$ra"};

CPU::CPU(uint32_t pc, Memory &iMem, Memory &dMem) : pc(pc), iMem(iMem), dMem(dMem) {
  for(int i = 0; i < NREGS; i++) {
    regFile[i] = 0;
  }
  hi = 0;
  lo = 0;
  regFile[28] = 0x10008000; // gp
  regFile[29] = 0x10000000 + dMem.getSize(); // sp

  instructions = 0;
  stop = false;
}

void CPU::run() {
  while(!stop) {
    instructions++;

    fetch();
    decode();
    execute();
    mem();
    writeback();
    stats.clock(IF1);

    

    D(printRegFile());
  }
}

void CPU::fetch() {
  instr = iMem.loadWord(pc);
  pc = pc + 4;
}


void CPU::decode() {
  uint32_t opcode;      // opcode field
  uint32_t rs, rt, rd;  // register specifiers
  uint32_t shamt;       // shift amount (R-type)
  uint32_t funct;       // funct field (R-type)
  uint32_t uimm;        // unsigned version of immediate (I-type)
  int32_t simm;         // signed version of immediate (I-type)
  uint32_t addr;        // jump address offset field (J-type)

  // shifting and masking to find values
  opcode = (instr & 0xfc000000) >> 26;
  rs = (instr & 0x3E00000) >> 21 ;
  rt = (instr & 0x1f0000) >> 16;
  rd = (instr & 0xf800) >> 11;
  shamt = (instr & 0x7c0) >> 6;
  funct = (instr & 0x3f);
  uimm = (instr & 0xffff);
  simm =  (instr & 0xffff) | ((instr & 0x8000) ? 0xffff0000 : 0); // checking for negative then doing 2s compliment if neccessary
  addr = (instr & 0x3ffffff);


  aluSrc1=0;
  aluSrc2=0;
  writeDest=false;
  writeData=false;
  opIsMultDiv=false;
  opIsLoad=false;
  opIsStore=false;
  aluSrc1=REG_ZERO;
  aluSrc2=REG_ZERO;
  destReg=REG_ZERO;
  storeData=REG_ZERO;
  aluOp=ADD;

  D(cout << "  " << hex << setw(8) << pc - 4 << ": ");
  switch(opcode) {
    case 0x00:
      switch(funct) {

        case 0x00: D(cout << "sll " << regNames[rd] << ", " << regNames[rs] << ", " << dec << shamt);
                   writeDest= true;
                   destReg= rd; stats.registerDest(destReg, MEM1);
                   aluOp= SHF_L;
                   stats.registerSrc(rs, EXE1);
                   aluSrc1= regFile[rs]; 
                   aluSrc2= shamt;
                   break; 

        case 0x03: D(cout << "sra " << regNames[rd] << ", " << regNames[rs] << ", " << dec << shamt);
                   writeDest= true;
                   destReg= rd; stats.registerDest(destReg, MEM1);
                   aluOp= SHF_R;
                   stats.registerSrc(rs, EXE1);
                   aluSrc1= regFile[rs]; 
                   aluSrc2= shamt;
                   break; 

        case 0x08: D(cout << "jr " << regNames[rs]);
                   writeDest= false; stats.registerDest(REG_ZERO, MEM1);
                   stats.registerSrc(rs, ID);
                   pc=regFile[rs]; 
                   stats.flush(ID - IF1); 
                   break;

        case 0x10: D(cout << "mfhi " << regNames[rd]);
                   writeDest= true;
                   destReg= rd; stats.registerDest(destReg, MEM1);
                   aluOp= ADD;
                   stats.registerSrc(REG_HILO, EXE1);
                   aluSrc1= hi; 
                   aluSrc2= regFile[REG_ZERO];
                   break;

        case 0x12: D(cout << "mflo " << regNames[rd]);
                   writeDest= true;
                   destReg= rd; stats.registerDest(destReg, MEM1);
                   aluOp= ADD;
                   stats.registerSrc(REG_HILO, EXE1);
                   aluSrc1= lo; 
                   aluSrc2= regFile[REG_ZERO];
                   break;

        case 0x18: D(cout << "mult " << regNames[rs] << ", " << regNames[rt]);
                   writeDest= false;
                   opIsMultDiv=true;
                   stats.registerDest(REG_HILO, WB);
                   aluOp= MUL;
                   stats.registerSrc(rs, EXE1);
                   stats.registerSrc(rt, EXE1);
                   aluSrc1= regFile[rs]; 
                   aluSrc2= regFile[rt];
                   break;

        case 0x1a: D(cout << "div " << regNames[rs] << ", " << regNames[rt]);
                   writeDest= false;
                   stats.registerDest(REG_HILO, WB);
                   aluOp= DIV;
                   stats.registerSrc(rs, EXE1);
                   stats.registerSrc(rt, EXE1);
                   opIsMultDiv=true;
                   aluSrc1= regFile[rs]; 
                   aluSrc2= regFile[rt];
                   break;

        case 0x21: D(cout << "addu " << regNames[rd] << ", " << regNames[rs] << ", " << regNames[rt]);
                   writeDest= true;
                   destReg= rd; stats.registerDest(destReg, MEM1);
                   aluOp= ADD;
                   stats.registerSrc(rs, EXE1);
                   stats.registerSrc(rt, EXE1);
                   aluSrc1= regFile[rs]; 
                   aluSrc2= regFile[rt];
                   break;

        case 0x23: D(cout << "subu " << regNames[rd] << ", " << regNames[rs] << ", " << regNames[rt]);
                   writeDest= true;
                   destReg= rd; stats.registerDest(destReg, MEM1);
                   aluOp= ADD;
                   stats.registerSrc(rs, EXE1);
                   stats.registerSrc(rt, EXE1);
                   aluSrc1= regFile[rs]; 
                   aluSrc2= -regFile[rt];
                   break; //hint: subtract is the same as adding a negative

        case 0x2a: D(cout << "slt " << regNames[rd] << ", " << regNames[rs] << ", " << regNames[rt]);
                   writeDest= true;
                   destReg= rd; stats.registerDest(destReg, MEM1);
                   aluOp= CMP_LT;
                   stats.registerSrc(rs, EXE1);
                   stats.registerSrc(rt, EXE1);
                   aluSrc1= regFile[rs]; 
                   aluSrc2= regFile[rt];
                   break;
        default: cerr << "unimplemented instruction: pc = 0x" << hex << pc - 4 << endl;
      }
      break;



    case 0x02: D(cout << "j " << hex << ((pc & 0xf0000000) | addr << 2));
               writeDest = false; 
               pc = (pc & 0xf0000000) | addr << 2;
               stats.flush(ID - IF1);
               break;

    case 0x03: D(cout << "jal " << hex << ((pc & 0xf0000000) | addr << 2));
               writeDest = true; destReg = REG_RA; stats.registerDest(destReg, EXE1);
               aluOp = ADD;
               aluSrc1 = pc;
               aluSrc2 = regFile[REG_ZERO];
               pc = (pc & 0xf0000000) | addr << 2;
               stats.flush(ID - IF1);
               break;

    case 0x04: D(cout << "beq " << regNames[rs] << ", " << regNames[rt] << ", " << pc + (simm << 2));
               stats.registerSrc(rs, ID);
               stats.registerSrc(rt, ID);
               if(regFile[rs] == regFile[rt]){
                  pc = pc + (simm << 2);
                  stats.flush(ID - IF1);
                  stats.countTaken();
               }
               stats.countBranch();
               break;  

    case 0x05: D(cout << "bne " << regNames[rs] << ", " << regNames[rt] << ", " << pc + (simm << 2));
               stats.registerSrc(rs, ID);
               stats.registerSrc(rt, ID);
               if(regFile[rs] != regFile[rt]){
                  pc = pc + (simm << 2);
                  stats.flush(ID - IF1);
                  stats.countTaken();
               }
               stats.countBranch();
               break;  

    case 0x09: D(cout << "addiu " << regNames[rt] << ", " << regNames[rs] << ", " << dec << simm);
               writeDest= true;
               destReg= rt; stats.registerDest(destReg, MEM1);
               aluOp= ADD;
               aluSrc1= regFile[rs]; stats.registerSrc(rs, EXE1);
               aluSrc2= simm;
               break;

    case 0x0c: D(cout << "andi " << regNames[rt] << ", " << regNames[rs] << ", " << dec << uimm);
               writeDest= true;
               destReg= rt; stats.registerDest(destReg, MEM1);
               aluOp= AND;
               aluSrc1= regFile[rs]; stats.registerSrc(rs, EXE1);
               aluSrc2= uimm;
               break;

    case 0x0f: D(cout << "lui " << regNames[rt] << ", " << dec << simm);
               writeDest= true;
               destReg= rt; stats.registerDest(destReg, MEM1);
               aluOp= SHF_L;
               aluSrc1= simm; stats.registerSrc(rs, EXE1);
               aluSrc2= 16;
               break; //use the ALU to perform necessary op, you may set aluSrc2 = xx directly

    case 0x1a: D(cout << "trap " << hex << addr);
               switch(addr & 0xf) {
                 case 0x0: cout << endl; break;
                 case 0x1: cout << " " << (signed)regFile[rs]; stats.registerSrc(rs, EXE1);
                           break;
                 case 0x5: cout << endl << "? "; cin >> regFile[rt]; stats.registerDest(rt, MEM1);
                           break;
                 case 0xa: stop = true; break;
                 default: cerr << "unimplemented trap: pc = 0x" << hex << pc - 4 << endl;
                          stop = true;
               }
               break;

    case 0x23: D(cout << "lw " << regNames[rt] << ", " << dec << simm << "(" << regNames[rs] << ")");
               writeDest= true;
               destReg= rt; 
               opIsLoad=true;
               aluOp= ADD;
               stats.registerDest(destReg, WB);
               aluSrc1= regFile[rs]; stats.registerSrc(rs, EXE1);
               aluSrc2= simm;
               stats.countMemOp();
               break;  // do not interact with memory here - setup control signals for mem()

    case 0x2b: D(cout << "sw " << regNames[rt] << ", " << dec << simm << "(" << regNames[rs] << ")");
               opIsStore=true; 
               storeData=regFile[rt];
               aluOp=ADD;
               aluSrc1= regFile[rs]; 
               aluSrc2= simm;
               stats.registerSrc(rs, EXE1);
               stats.registerSrc(rt, MEM1);
               stats.countMemOp();
               break;  // same comment as lw

    default: cerr << "unimplemented instruction: pc = 0x" << hex << pc - 4 << endl;
  }
  D(cout << endl);
}

void CPU::execute() {
  aluOut = alu.op(aluOp, aluSrc1, aluSrc2);
}

void CPU::mem() {
  if(opIsLoad){
    writeData = dMem.loadWord(aluOut);
    stats.stall(cache.access(aluOut, LOAD));
  }
  else
    writeData = aluOut;

  if(opIsStore){
    dMem.storeWord(storeData, aluOut);
    stats.stall(cache.access(aluOut, STORE));
  }
}

void CPU::writeback() {
  if(writeDest && destReg > 0) // skip when write is to zero_register
    regFile[destReg] = writeData;
  
  if(opIsMultDiv) {
    hi = alu.getUpper();
    lo = alu.getLower();
  }
}

void CPU::printRegFile() {
  cout << hex;
  for(int i = 0; i < NREGS; i++) {
    cout << "    " << regNames[i];
    if(i > 0) cout << "  ";
    cout << ": " << setfill('0') << setw(8) << regFile[i];
    if( i == (NREGS - 1) || (i + 1) % 4 == 0 )
      cout << endl;
  }
  cout << "    hi   : " << setfill('0') << setw(8) << hi;
  cout << "    lo   : " << setfill('0') << setw(8) << lo;
  cout << dec << endl;
}

void CPU::printFinalStats() {

  int totalHazards = stats.getRAWHazards();


  cout << "Program finished at pc = 0x" << hex << pc << "  ("
       << dec << instructions << " instructions executed)" << endl << endl;

  cout << "Cycles: " << stats.getCycles() << endl;
  cout << "CPI: " <<fixed<<setprecision(2)<<(stats.getCycles()/static_cast<double>(instructions)) << endl << endl;

  cout << "Bubbles: " << stats.getBubbles() << endl;
  cout << "Flushes: " << stats.getFlushes() << endl;
  cout << "Stalls: " << stats.getStalls() << endl << endl;

  cache.printFinalStats();

/*
  cout <<"RAW hazards: " << totalHazards << fixed << setprecision(2) << " (" << "1 per every "<< instructions/float(stats.getRAWHazards()) <<" instructions)"<<endl;
  cout << "On EXE1 op: " << stats.hazardStages[EXE1]<< fixed << setprecision(0) <<" (" <<float(stats.hazardStages[EXE1])/float(totalHazards)*100<<"%)"<< endl;
  cout << "On EXE2 op: " << stats.hazardStages[EXE2]<< fixed << setprecision(0) <<" (" <<float(stats.hazardStages[EXE2])/float(totalHazards)*100<<"%)"<< endl;
  cout << "On MEM1 op: " << stats.hazardStages[MEM1]<< fixed << setprecision(0) <<" (" <<float(stats.hazardStages[MEM1])/float(totalHazards)*100<<"%)"<< endl;
  cout << "On MEM2 op: " << stats.hazardStages[MEM2]<< fixed << setprecision(0) <<" (" <<float(stats.hazardStages[MEM2])/float(totalHazards)*100<<"%)"<< endl;

 */
}
